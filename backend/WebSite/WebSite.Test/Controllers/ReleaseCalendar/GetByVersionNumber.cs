﻿namespace WebSite.Test.Controllers.ReleaseCalendar
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Moq;
    using System;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Cache;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service.Interface;
    using WebSite.Controllers;
    using WebSite.Models;
    using WebSite.Service;
    using Xunit;

    public class GetByVersionNumber
    {
        private readonly Mock<IReleaseCalendarService> _releaseCalendarService;
        private readonly Mock<IReleaseCalendarInformationCache> _releaseCalendarInformationCache;
        private readonly Mock<ILogger<ReleaseCalendarController>> _logger;
        private readonly Mock<ITimeService> _timeService;

        private readonly ReleaseCalendarController _controller;

        public GetByVersionNumber()
        {
            _releaseCalendarService = new Mock<IReleaseCalendarService>();
            _releaseCalendarInformationCache = new Mock<IReleaseCalendarInformationCache>();
            _logger = new Mock<ILogger<ReleaseCalendarController>>();

            _timeService = new Mock<ITimeService>();
            _timeService.Setup(t => t.GetCurrentDate()).Returns(DateTime.Now);

            _controller = new ReleaseCalendarController(
                                        _releaseCalendarService.Object,
                                        _releaseCalendarInformationCache.Object,
                                        _timeService.Object,
                                        _logger.Object);
        }

        [Fact]
        public void ItExists()
        {
            // Arrange
            var service = new Mock<IReleaseCalendarService>();
            var cache = new Mock<IReleaseCalendarInformationCache>();
            var time = new Mock<ITimeService>();
            var logger = new Mock<ILogger<ReleaseCalendarController>>();

            var controller = new ReleaseCalendarController(service.Object, cache.Object, time.Object, logger.Object);
            var versionNumber = new VersionNumberGetRequest();

            // Act & Assert
            var result = controller.GetByVersionNumber(versionNumber);
        }

        [Fact]
        public void SearchingAVersionNumberWhichNotExistsShouldReturnNotFound()
        {
            // Arrange
            var versionNumberGetRequest = new VersionNumberGetRequest
            {
                Week = 1,
                Quarter = 1,
                Year = 12
            };

            var versionNumber = new VersionNumber
            {
                Week = 1,
                Quarter = 1,
                Year = 12
            };

            _releaseCalendarService
                .Setup(r => r.GetReleaseInformationByVersionNumber(versionNumber))
                .Returns(Task.FromResult<ReleaseInformation>(null));

            // Act
            var result = _controller.GetByVersionNumber(versionNumberGetRequest);

            // Assert
            var actionResult = Assert.IsType<Task<ActionResult<ReleaseInformationGetResponse>>>(result);
            Assert.IsType<NotFoundResult>(actionResult.Result.Result);
        }

        [Fact]
        public void SearchingAVersionNumberWhichExistsShouldReturnVersionNumber()
        {
            // Arrange
            var versionNumberGetRequest = new VersionNumberGetRequest
            {
                Week = 1,
                Quarter = 1,
                Year = 12
            };

            var versionNumber = new VersionNumber
            {
                Week = 1,
                Quarter = 1,
                Year = 12
            };

            var releaseInformationReturned = new ReleaseInformation
            {
                VersionNumber = versionNumber,
            };

            string expectedVersionNumber = versionNumber.ToString();

            _releaseCalendarService
                .Setup(r => r.GetReleaseInformationByVersionNumber(versionNumber))
                .Returns(Task.FromResult(releaseInformationReturned));

            // Act
            var result = _controller.GetByVersionNumber(versionNumberGetRequest);

            // Assert
            Task<ActionResult<ReleaseInformationGetResponse>> actionResult = Assert.IsType<Task<ActionResult<ReleaseInformationGetResponse>>>(result);
            Assert.Equal(expectedVersionNumber, actionResult.Result.Value.Version);
        }
    }
}