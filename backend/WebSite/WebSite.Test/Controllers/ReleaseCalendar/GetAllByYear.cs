namespace WebSite.Test.Controllers.ReleaseCalendar
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Cache;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service.Interface;
    using WebSite.Controllers;
    using WebSite.Models;
    using WebSite.Service;
    using Xunit;

    public class GetAllByYear
    {
        private ReleaseCalendarController _releaseCalendarController;
        private const short YEAR_2020 = 2020;

        private readonly Mock<IReleaseCalendarService> _releaseCalendarService;
        private readonly Mock<IReleaseCalendarInformationCache> _releaseCalendarInformationCache;
        private readonly Mock<ILogger<ReleaseCalendarController>> _logger;
        private readonly Mock<ITimeService> _timeService;

        public GetAllByYear()
        {
            _releaseCalendarService = new Mock<IReleaseCalendarService>();
            _releaseCalendarInformationCache = new Mock<IReleaseCalendarInformationCache>();

            _logger = new Mock<ILogger<ReleaseCalendarController>>();
            _timeService = new Mock<ITimeService>();
            _timeService.Setup(t => t.GetCurrentDate()).Returns(new DateTime(YEAR_2020, 1, 1));

            _releaseCalendarController = new ReleaseCalendarController(
                                                _releaseCalendarService.Object, 
                                                _releaseCalendarInformationCache.Object,
                                                _timeService.Object,
                                                _logger.Object);
        }

        [Fact]
        public void ItExists()
        {
            // Arrange
            var service = new Mock<IReleaseCalendarService>();
            var cache = new Mock<IReleaseCalendarInformationCache>();
            var time = new Mock<ITimeService>();
            var logger = new Mock<ILogger<ReleaseCalendarController>>();

            var controller = new ReleaseCalendarController(service.Object, cache.Object, time.Object, logger.Object);

            // Act & Assert
            var result = controller.GetAllByYear(YEAR_2020);
        }

        [Fact]
        public void ItReturnsIEnumerableOfReleaseInformationGetResponse()
        {
            // Arrange
            _releaseCalendarService
                .Setup(r => r.OfTheYear(YEAR_2020))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(YEAR_2020, Enumerable.Empty<ReleaseInformation>())));

            // Act
            var result = _releaseCalendarController.GetAllByYear(YEAR_2020);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<Task<IEnumerable<ReleaseInformationGetResponse>>>(result);
        }

        [Fact]
        public async void ItReturnsCollectionOfReleaseSummary()
        {
            // Arrange

            _releaseCalendarService
                .Setup(r => r.OfTheYear(YEAR_2020))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(YEAR_2020, Enumerable.Empty<ReleaseInformation>())));

            // Act
            IEnumerable<ReleaseInformationGetResponse> result = await _releaseCalendarController.GetAllByYear(YEAR_2020);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<ReleaseInformationGetResponse>>(result);
        }

        [Fact]
        public async void ItCallsGetAllServiceOnce()
        {
            // Arrange
            _releaseCalendarService.Setup(r => r.OfTheYear(YEAR_2020))
                                        .Returns(Task.FromResult(new ReleaseCalendarInformation(YEAR_2020, Enumerable.Empty<ReleaseInformation>())));


            // Act
            OkObjectResult result = await _releaseCalendarController.GetAllByYear(YEAR_2020) as OkObjectResult;

            // Assert
            _releaseCalendarService.Verify(r => r.OfTheYear(YEAR_2020), Times.Once);
        }
    }
}