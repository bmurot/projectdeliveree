﻿namespace WebSite.Test.Controllers.ReleaseCalendar
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Talentsoft.ReleaseInformation;
    using Talentsoft.ReleaseInformation.Cache;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Talentsoft.ReleaseInformation.Service.Interface;
    using WebSite.Controllers;
    using WebSite.Models;
    using WebSite.Service;
    using Xunit;

    public class GetAllVersionsOfInterest
    {
        private ReleaseCalendarController _releaseCalendarController;

        private readonly Mock<IReleaseCalendarService> _releaseCalendarService;
        private readonly Mock<IReleaseCalendarInformationCache> _releaseCalendarInformationCache;
        private readonly Mock<ITimeService> _timeService;
        private readonly Mock<ILogger<ReleaseCalendarController>> _logger;

        public GetAllVersionsOfInterest()
        {
            _releaseCalendarService = new Mock<IReleaseCalendarService>();
            _releaseCalendarInformationCache = new Mock<IReleaseCalendarInformationCache>();
            _timeService = new Mock<ITimeService>();
            _logger = new Mock<ILogger<ReleaseCalendarController>>();

            _releaseCalendarController = new ReleaseCalendarController(
                                                _releaseCalendarService.Object,
                                                _releaseCalendarInformationCache.Object,
                                                _timeService.Object,
                                                _logger.Object);
        }

        [Fact(Skip = "Don't want to initialize mocks")]
        public async void ItExists()
        {
            // Arrange
            var service = new Mock<IReleaseCalendarService>();
            var cache = new Mock<IReleaseCalendarInformationCache>();
            var time = new Mock<ITimeService>();
            var logger = new Mock<ILogger<ReleaseCalendarController>>();
            var controller = new ReleaseCalendarController(service.Object, cache.Object, time.Object, logger.Object);

            // Act && Assert
            IEnumerable<ReleaseInformationGetResponse> result = await controller.GetAllVersionsOfInterest();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async void GivenBeginningMonthsShouldReturnPrecedentYearAndCurrentYear(int month)
        {
            // Arrange
            var culture = new CultureInfo("fr-FR");
            _timeService.Setup(t => t.GetCurrentDate()).Returns(new DateTime(DateTime.Now.Year, month, 1));

            short currentYear = (short)_timeService.Object.GetCurrentDate().Year;
            short previousYear = (short)(currentYear - 1);

            Versionning firstVersionningForCurrentYear = CreateFirstVersionningOfAYear(culture, currentYear);
            IEnumerable<ReleaseInformation> currentReleases = CreateReleasesWithFirstVersionningOfYear(firstVersionningForCurrentYear);

            Versionning firstVersionningForPreviousYear = CreateFirstVersionningOfAYear(culture, previousYear);
            IEnumerable<ReleaseInformation> previousReleases = CreateReleasesWithFirstVersionningOfYear(firstVersionningForPreviousYear);

            _releaseCalendarService
                .Setup(r => r.OfTheYear(currentYear))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(currentYear, currentReleases)));

            _releaseCalendarService
                .Setup(r => r.OfTheYear(previousYear))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(previousYear, previousReleases)));

            // Act
            IEnumerable<ReleaseInformationGetResponse> result = await _releaseCalendarController.GetAllVersionsOfInterest();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            var resultArray = result.ToArray();
            string previousVersion = resultArray[0].Version;
            string currentVersion = resultArray[1].Version;
            Assert.StartsWith(firstVersionningForPreviousYear.GetVersionNumber().Year.ToString(), previousVersion);
            Assert.StartsWith(firstVersionningForCurrentYear.GetVersionNumber().Year.ToString(), currentVersion);
        }

        [Theory]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        public async void GivenIntermediateMonthShouldReturnCurrentYear(int month)
        {
            // Arrange
            var culture = new CultureInfo("fr-FR");
            _timeService.Setup(t => t.GetCurrentDate()).Returns(new DateTime(DateTime.Now.Year, month, 1));

            short currentYear = (short)_timeService.Object.GetCurrentDate().Year;

            Versionning firstVersionningForCurrentYear = CreateFirstVersionningOfAYear(culture, currentYear);
            IEnumerable<ReleaseInformation> currentReleases = CreateReleasesWithFirstVersionningOfYear(firstVersionningForCurrentYear);

            _releaseCalendarService
                .Setup(r => r.OfTheYear(currentYear))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(currentYear, currentReleases)));

            // Act
            IEnumerable<ReleaseInformationGetResponse> result = await _releaseCalendarController.GetAllVersionsOfInterest();

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);
            var resultArray = result.ToArray();
            string currentVersion = resultArray[0].Version;
            Assert.StartsWith(firstVersionningForCurrentYear.GetVersionNumber().Year.ToString(), currentVersion);
        }

        [Theory]
        [InlineData(9)]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(12)]
        public async void GivenEndMonthsShouldReturnCurrentYearAndNextYear(int month)
        {
            // Arrange
            var culture = new CultureInfo("fr-FR");
            _timeService.Setup(t => t.GetCurrentDate()).Returns(new DateTime(DateTime.Now.Year, month, 1));

            short currentYear = (short)_timeService.Object.GetCurrentDate().Year;
            short nextYear = (short)(currentYear + 1);
            
            Versionning firstVersionningForCurrentYear = CreateFirstVersionningOfAYear(culture, currentYear);
            IEnumerable<ReleaseInformation> currentReleases = CreateReleasesWithFirstVersionningOfYear(firstVersionningForCurrentYear);

            Versionning firstVersionningForNextYear = CreateFirstVersionningOfAYear(culture, nextYear);
            IEnumerable<ReleaseInformation> nextReleases = CreateReleasesWithFirstVersionningOfYear(firstVersionningForNextYear);

            _releaseCalendarService
                .Setup(r => r.OfTheYear(currentYear))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(currentYear, currentReleases)));

            _releaseCalendarService
                .Setup(r => r.OfTheYear(nextYear))
                .Returns(Task.FromResult(new ReleaseCalendarInformation(nextYear, nextReleases)));

            // Act
            IEnumerable<ReleaseInformationGetResponse> result = await _releaseCalendarController.GetAllVersionsOfInterest();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            var resultArray = result.ToArray();
            string currentVersion = resultArray[0].Version;
            string nextVersion = resultArray[1].Version;
            Assert.StartsWith(firstVersionningForCurrentYear.GetVersionNumber().Year.ToString(), currentVersion);
            Assert.StartsWith(firstVersionningForNextYear.GetVersionNumber().Year.ToString(), nextVersion);
        }

        private IEnumerable<ReleaseInformation> CreateReleasesWithFirstVersionningOfYear(Versionning versionning)
        {
            VersionNumber currentVersionNumber = versionning.GetVersionNumber();

            return new List<ReleaseInformation>
            {
                new ReleaseInformation
                {
                    VersionNumber = currentVersionNumber
                }
            };
        }

        private Versionning CreateFirstVersionningOfAYear(CultureInfo culture, short currentYear)
        {
            var currentReleaseWeek = new DateTime(
                                                    _timeService.Object.GetCurrentDate().Year,
                                                    _timeService.Object.GetCurrentDate().Month,
                                                    _timeService.Object.GetCurrentDate().Day);
            DateTime.SpecifyKind(currentReleaseWeek, DateTimeKind.Local);
            DateTime firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(currentYear, culture);

            return new Versionning(firstYearWeek, firstYearWeek, culture);
        }
    }
}