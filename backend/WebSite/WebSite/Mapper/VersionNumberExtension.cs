﻿namespace WebSite.Mapper
{
    using Talentsoft.ReleaseInformation.Entities;
    using WebSite.Models;

    public static class VersionNumberExtension
    {
        public static VersionNumber Convert(this VersionNumberGetRequest versionNumberGetRequest)
        {
            return new VersionNumber
            {
                Week = versionNumberGetRequest.Week,
                Quarter = versionNumberGetRequest.Quarter,
                Year = versionNumberGetRequest.Year
            };
        }
    }
}