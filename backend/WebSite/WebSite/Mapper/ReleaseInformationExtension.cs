﻿namespace WebSite.Mapper
{
    using System;
    using Talentsoft.ReleaseInformation.Entities;
    using WebSite.Models;

    public static class ReleaseInformationExtension
    {
        public static ReleaseInformationGetResponse ConvertToModel(this ReleaseInformation release)
        {
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

            return ConvertToModel(release, timeZoneInfo);
        }

        public static ReleaseInformationGetResponse ConvertToModel(
                                this ReleaseInformation release,
                                TimeZoneInfo timeZoneInfo)
        {
            return new ReleaseInformationGetResponse
            {
                ReleaseDate = release.ProductionDate.ConvertFromUtcTo(timeZoneInfo),
                Version = release.VersionNumber.ToString(),
                Comment = release.Comment ?? string.Empty,
                QualificationStartDate = release.QualificationStartDate.ConvertFromUtcTo(timeZoneInfo),
                QualificationEndDate = release.QualificationEndDate.ConvertFromUtcTo(timeZoneInfo),
                DateOfQualificationPackageCreation = release.DateOfQualificationPackageCreation.ConvertFromUtcTo(timeZoneInfo),
                DevStartDate = release.DevStartDate.ConvertFromUtcTo(timeZoneInfo),
                DevEndDate = release.DevEndDate.ConvertFromUtcTo(timeZoneInfo),
                JiraFilterLink = CreateJiraFilterLink(release.VersionNumber.ToString())
            };
        }

        private static string CreateJiraFilterLink(string versionNumber)
        {
            return $"https://talentsoft.atlassian.net/issues/?jql=project%20IN%20(Career%2C%20Recruiting%2C%20HA%2C%20LMS%2C%20LCMS)%20AND%20fixVersion%20%3D%20{versionNumber}%20AND%20(issuetype%20in%20(Epic%2C%20Story%2C%20%22Technical%20Debt%22%2C%20Task%2C%20Bug)%20OR%20(issuetype%20%3D%20Bug%20AND%20Patch%20%3D%20Production)%20)%20ORDER%20BY%20Project%20DESC%2C%20%22Product%20Line%22%2C%20issuetype&startIndex=50";
        }
    }
}