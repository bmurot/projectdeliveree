﻿namespace WebSite.Mapper
{
    using System;

    public static class DateTimeExtension
    {
        public static DateTime ConvertFromUtcTo(this DateTime utcDate, TimeZoneInfo timeZoneInfo)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZoneInfo);
        }
    }
}