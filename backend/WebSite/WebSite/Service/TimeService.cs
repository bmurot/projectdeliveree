﻿namespace WebSite.Service
{
    using System;

    public class TimeService : ITimeService
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }
    }
}