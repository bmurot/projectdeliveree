﻿namespace WebSite.Service
{
    using System;

    public interface ITimeService
    {
        DateTime GetCurrentDate();
    }
}