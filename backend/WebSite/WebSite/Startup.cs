﻿namespace WebSite
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Talentsoft.ReleaseInformation.Cache;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Repository;
    using Talentsoft.ReleaseInformation.Service;
    using Talentsoft.ReleaseInformation.Service.Interface;
    using WebSite.Service;

    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddTransient<IRepository<ReleaseCalendarInformation>, ReleaseCalendarRepository>(r => new ReleaseCalendarRepository(Configuration["ConnectionStrings:DefaultConnection"], Configuration["DatabaseName"]));
            services.AddTransient<IReleaseCalendarService, ReleaseCalendarService>();
            services.AddTransient<ITimeService, TimeService>();

            services.AddCors(options =>
                options.AddPolicy("CorsPolicy", builder =>
                builder.AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .WithOrigins(Configuration["Frontend"])));

            services.AddMemoryCache();
            services.AddSingleton<IReleaseCalendarInformationCache, ReleaseCalendarInformationCache>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("CorsPolicy");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
