﻿namespace WebSite.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Cache;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service.Interface;
    using WebSite.Mapper;
    using WebSite.Models;
    using WebSite.Service;

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReleaseCalendarController : ControllerBase
    {
        private readonly IReleaseCalendarService _releaseCalendarService;
        private readonly IReleaseCalendarInformationCache _releaseCalendarInformationCache;
        private readonly ITimeService _timeService;
        private readonly ILogger _logger;

        public ReleaseCalendarController(
            IReleaseCalendarService releaseCalendarService,
            IReleaseCalendarInformationCache releaseCalendarInformationCache,
            ITimeService timeService,
            ILogger<ReleaseCalendarController> logger)
        {
            _releaseCalendarService = releaseCalendarService;
            _releaseCalendarInformationCache = releaseCalendarInformationCache;
            _timeService = timeService;
            _logger = logger;
        }

        [HttpGet("{year}")]
        public async Task<IEnumerable<ReleaseInformationGetResponse>> GetAllByYear(short year)
        {
            _logger.LogInformation("/** ReleaseCalendarController **/ - GetAllByYear");

            ReleaseCalendarInformation releaseCalendar = await GetReleaseCalendarFor(year);

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");

            return releaseCalendar.ReleaseInformation.Select(r => r.ConvertToModel(timeZoneInfo));
        }

        /**
         * Versions of interest are the ones of current year, and :
         * Previous year when two first months of the year
         * Next year when four last months of the year
         * **/
        [HttpGet("all")]
        public async Task<IEnumerable<ReleaseInformationGetResponse>> GetAllVersionsOfInterest()
        {
            _logger.LogInformation("/** ReleaseCalendarController **/ - GetAllVersionsOfInterest");

            IEnumerable<ReleaseInformationGetResponse> responseContent = null;

            try
            {
                TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");
                int currentMonth = _timeService.GetCurrentDate().Month;
                short currentYear = (short)_timeService.GetCurrentDate().Year;

                ReleaseCalendarInformation currentYearReleaseCalendar = await GetReleaseCalendarFor(currentYear);

                IEnumerable<ReleaseInformation> previousAndCurrentYear = await GetPreviousAndCurrentYear(currentMonth, currentYear, currentYearReleaseCalendar);

                if (previousAndCurrentYear == null)
                {
                    IEnumerable<ReleaseInformation> currentAndNextYear = await GetCurrentAndNextYear(currentMonth, currentYear, currentYearReleaseCalendar);

                    if (currentAndNextYear != null)
                    {
                        return currentAndNextYear.Select(r => r.ConvertToModel(timeZoneInfo));
                    }
                }
                else
                {
                    return previousAndCurrentYear.Select(r => r.ConvertToModel(timeZoneInfo));
                }

                responseContent = currentYearReleaseCalendar.ReleaseInformation.Select(r => r.ConvertToModel(timeZoneInfo));
            }
            catch (SystemException ex)
            {
                _logger.LogError(ex.Message);
            }
            

            return responseContent;
        }

        private async Task<IEnumerable<ReleaseInformation>> GetPreviousAndCurrentYear(
                                                                    int currentMonth,
                                                                    short currentYear,
                                                                    ReleaseCalendarInformation currentYearReleaseCalendar)
        {
            IEnumerable<ReleaseInformation> releaseInformationForPreviousAndCurrentYear = null;

            if (currentMonth <= 2)
            {
                var previousYear = (short)(currentYear - 1);

                releaseInformationForPreviousAndCurrentYear = await GetReleaseInformationForTwoYears(
                                                                                    previousYear,
                                                                                    currentYearReleaseCalendar);
            }

            return releaseInformationForPreviousAndCurrentYear;
        }

        private async Task<IEnumerable<ReleaseInformation>> GetCurrentAndNextYear(
                                                                    int currentMonth,
                                                                    short currentYear,
                                                                    ReleaseCalendarInformation currentYearReleaseCalendar)
        {
            IEnumerable<ReleaseInformation> releaseInformationForCurrentAndNextYear = null;

            if (currentMonth >= 9)
            {
                var nextYear = (short)(currentYear + 1);

                releaseInformationForCurrentAndNextYear = await GetReleaseInformationForTwoYears(
                                                                                    nextYear,
                                                                                    currentYearReleaseCalendar);
            }

            return releaseInformationForCurrentAndNextYear;
        }

        private async Task<IEnumerable<ReleaseInformation>> GetReleaseInformationForTwoYears(
                                                                    short yearToMerge,
                                                                    ReleaseCalendarInformation currentReleaseCalendarYear)
        {
            ReleaseCalendarInformation releaseCalendarYearToMerge = await GetReleaseCalendarFor(yearToMerge);
            //order union by year order.
            if (releaseCalendarYearToMerge.Year > currentReleaseCalendarYear.Year)
            {
                return currentReleaseCalendarYear.ReleaseInformation.Union(releaseCalendarYearToMerge.ReleaseInformation);
            }
            return releaseCalendarYearToMerge.ReleaseInformation.Union(currentReleaseCalendarYear.ReleaseInformation);
        }

        private async Task<ReleaseCalendarInformation> GetReleaseCalendarFor(short currentYear)
        {
            ReleaseCalendarInformation currentYearReleaseCalendar = _releaseCalendarInformationCache.GetForYear(currentYear);

            if (currentYearReleaseCalendar == null)
            {
                currentYearReleaseCalendar = await _releaseCalendarService.OfTheYear(currentYear);
                _releaseCalendarInformationCache.SetForYear(currentYearReleaseCalendar);
            }

            return currentYearReleaseCalendar;
        }

        [HttpGet]
        public async Task<ActionResult<ReleaseInformationGetResponse>> GetByVersionNumber([FromBody]VersionNumberGetRequest versionNumber)
        {
            ReleaseInformation releaseOfVersionNumber = await _releaseCalendarService.GetReleaseInformationByVersionNumber(versionNumber.Convert());

            if (releaseOfVersionNumber == null)
            {
                return NotFound();
            }

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");

            return releaseOfVersionNumber.ConvertToModel(timeZoneInfo);
        }
    }
}