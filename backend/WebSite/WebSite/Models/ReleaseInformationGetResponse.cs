﻿namespace WebSite.Models
{
    using System;

    public class ReleaseInformationGetResponse
    {
        public DateTime ReleaseDate { get; set; }

        public string Version { get; set; }

        public string Comment { get; set; }

        public DateTime QualificationStartDate { get; set; }

        public DateTime QualificationEndDate { get; set; }

        public DateTime DateOfQualificationPackageCreation { get; set; }

        public DateTime DevStartDate { get; set; }

        public DateTime DevEndDate { get; set; }

        public string JiraFilterLink { get; set; }
    }
}