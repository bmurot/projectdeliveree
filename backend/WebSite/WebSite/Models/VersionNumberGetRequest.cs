﻿namespace WebSite.Models
{
    public class VersionNumberGetRequest
    {
        public int Year { get; set; }

        public int Quarter { get; set; }

        public int Week { get; set; }
    }
}