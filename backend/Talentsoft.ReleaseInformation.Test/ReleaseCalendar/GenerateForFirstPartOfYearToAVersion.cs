﻿namespace Talentsoft.ReleaseInformation.Test.ReleaseCalendar
{
    using System.Collections.Generic;
    using System.Globalization;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Xunit;

    public class GenerateForFirstPartOfYearToAVersion
    {
        [Fact]
        public void ItExists()
        {
            // Arrange
            var toVersionNumber = new VersionNumber
            {
                Year = 14,
                Quarter = 1,
                Week = 10
            };

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");


            // ACt && Assert
            IEnumerable<ReleaseInformation> result = releaseCalendar.GenerateForFirstPartOfYearToAVersion(toVersionNumber, culture);
        }

        [Fact]
        public void GivenAversionNumber_ShouldReturnACalendarToExpectedVersion()
        {
            // Arrange
            var toVersionNumber = new VersionNumber
            {
                Year = 14,
                Quarter = 1,
                Week = 0
            };

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");

            // Act
            IEnumerable<ReleaseInformation> result = releaseCalendar.GenerateForFirstPartOfYearToAVersion(toVersionNumber, culture);

            // Assert
            Assert.Single(result);
        }
    }
}