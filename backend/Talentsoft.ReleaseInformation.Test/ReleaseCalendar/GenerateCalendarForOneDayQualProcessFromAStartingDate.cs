﻿namespace Talentsoft.ReleaseInformation.Test.ReleaseCalendar
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Xunit;

    public class GenerateCalendarForOneDayQualProcessFromAStartingDate
    {
        [Fact]
        public void ItExists()
        {
            // Arrange
            var from = new DateTime(2021, 3, 22);
            var to = new DateTime(2021, 12, 27);

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");


            // Act && Assert
            IEnumerable<ReleaseInformation> result = releaseCalendar.GenerateCalendarForOneDayQualProcessFromAStartingDate(from, culture);
        }

        [Fact]
        public void GivenAStartDateWhichIsTheLasWeekOfYear_ShouldReturnLastVersionOfYear()
        {
            // Arrange
            var from = new DateTime(2021, 12, 27);

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");
            var expected = new VersionNumber
            {
                Year = 14,
                Quarter = 4,
                Week = 12
            };

            // Act
            IEnumerable<ReleaseInformation> result = releaseCalendar.GenerateCalendarForOneDayQualProcessFromAStartingDate(from, culture);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal(expected, result.First().VersionNumber);
        }

        [Fact]
        public void GivenAStartDateWhichIsTheWeekBeforeLastYear_ShouldReturnLastVersionOfYear()
        {
            // Arrange
            var from = new DateTime(2021, 12, 20);

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");
            var expected = new VersionNumber
            {
                Year = 14,
                Quarter = 4,
                Week = 11
            };

            // Act
            IEnumerable<ReleaseInformation> result = releaseCalendar.GenerateCalendarForOneDayQualProcessFromAStartingDate(from, culture);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            Assert.Equal(expected, result.First().VersionNumber);
        }
    }
}