﻿namespace Talentsoft.ReleaseInformation.Test.ReleaseCalendar
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Xunit;

    public class Generate
    {
        [Fact]
        public async void ItExists()
        {
            // Arrange
            int year = 2020;

            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");

            // ACt && Assert
            IEnumerable<ReleaseInformation> result = await releaseCalendar.Generate(year, culture);
        }

        [Fact(Skip = "Used as a step for TDD")]
        public async void GivenAYearShouldReturnReleaseInformationOfFirstWeek()
        {
            // Arrange
            int year = 2018;
            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");
            var expectedVersion = new VersionNumber
            {
                Year = 11,
                Quarter = 1,
                Week = 1
            };
            

            // Act
            IEnumerable<ReleaseInformation> result = await releaseCalendar.Generate(year, culture);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);
            ReleaseInformation actualReleaseInformation = result.First();
            Assert.Equal(expectedVersion.Year, actualReleaseInformation.VersionNumber.Year);
            Assert.Equal(expectedVersion.Quarter, actualReleaseInformation.VersionNumber.Quarter);
            Assert.Equal(expectedVersion.Week, actualReleaseInformation.VersionNumber.Week);
        }

        [Fact]
        public async void GivenAYearShouldReturnReleaseInformationOfTwoWeeks()
        {
            // Arrange
            int year = 2018;
            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");
            var expectedVersion = new VersionNumber
            {
                Year = 11,
                Quarter = 1,
                Week = 3
            };

            var expectedReleaseInformation = new ReleaseInformation
            {
                VersionNumber = expectedVersion,
                ProductionDate = new DateTime(2018, 1, 22),
                QualificationStartDate = new DateTime(2018, 1, 15),
                QualificationEndDate = new DateTime(2018, 1, 18),
                DateOfQualificationPackageCreation = new DateTime(2018, 1, 12),
                DevStartDate = new DateTime(2018, 1, 8),
                DevEndDate = new DateTime(2018, 1, 11)
            };

            // Act
            List<ReleaseInformation> result = (await releaseCalendar.Generate(year, culture)).ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.Count());
            ReleaseInformation actualReleaseInformation = result[3];
            Assert.Equal(expectedReleaseInformation.VersionNumber.Year, actualReleaseInformation.VersionNumber.Year);
            Assert.Equal(expectedReleaseInformation.VersionNumber.Quarter, actualReleaseInformation.VersionNumber.Quarter);
            Assert.Equal(expectedReleaseInformation.VersionNumber.Week, actualReleaseInformation.VersionNumber.Week);
            Assert.Equal(expectedReleaseInformation.ProductionDate, actualReleaseInformation.ProductionDate);
            Assert.Equal(expectedReleaseInformation.QualificationStartDate, actualReleaseInformation.QualificationStartDate);
            Assert.Equal(expectedReleaseInformation.QualificationEndDate, actualReleaseInformation.QualificationEndDate);
            Assert.Equal(expectedReleaseInformation.DateOfQualificationPackageCreation, actualReleaseInformation.DateOfQualificationPackageCreation);
            Assert.Equal(expectedReleaseInformation.DevStartDate, actualReleaseInformation.DevStartDate);
            Assert.Equal(expectedReleaseInformation.DevEndDate, actualReleaseInformation.DevEndDate);
        }

        [Fact]
        public async void GivenAYearShouldReturnReleaseInformationOfOneWeek()
        {
            // Arrange
            int year = 2020;
            var releaseCalendar = new ReleaseCalendar();
            var culture = new CultureInfo("fr-FR");
            var expectedVersion = new VersionNumber
            {
                Year = 13,
                Quarter = 1,
                Week = 0
            };

            // Act
            List<ReleaseInformation> result = (await releaseCalendar.Generate(year, culture)).ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.Count());
            ReleaseInformation actualReleaseInformation = result[0];
            Assert.Equal(expectedVersion, actualReleaseInformation.VersionNumber);
        }
    }
}