﻿namespace Talentsoft.ReleaseInformation.Test.Service.VersionNumber
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Xunit;

    public class GetVersionNumber
    {
        [Fact]
        public void ItExists()
        {
            var releaseDate = DateTime.Now;
            var firstYearWeek = DateTime.Now;
            var versionning = new Versionning(releaseDate, firstYearWeek);

            VersionNumber versionNumber = versionning.GetVersionNumber();
        }

        [Fact]
        public void GivenYear2007ShouldReturnOneForYearNumber()
        {
            // Arrange
            var releaseDate = new DateTime(2007, 1, 1);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(0, result.Year);
        }

        [Fact]
        public void GivenYear2008ShouldReturnOneForYearNumber()
        {
            // Arrange
            var releaseDate = new DateTime(2008, 1, 1);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(1, result.Year);
        }

        [Fact]
        public void GivenYear2016ShouldReturnNineForYearNumber()
        {
            // Arrange
            var releaseDate = new DateTime(2016, 1, 1);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(9, result.Year);
        }

        [Fact]
        public void GivenFirstPartOfFirstQuarterShouldReturnOneForQuarter()
        {
            // Arrange
            var releaseDate = new DateTime(2019, 1, 8);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(1, result.Quarter);
        }

        [Fact]
        public void GivenFirstPartOfSecondQuarterShouldReturnTwoForQuarter()
        {
            // Arrange
            var releaseDate = new DateTime(2019, 4, 1);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(2, result.Quarter);
        }

        [Fact]
        public void GivenFirstPartOfThirdQuarterShouldReturnThreeForQuarter()
        {
            // Arrange
            var releaseDate = new DateTime(2019, 7, 1);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(3, result.Quarter);
        }

        [Fact]
        public void GivenFirstPartOfFourthQuarterShouldReturnFourForQuarter()
        {
            // Arrange
            var releaseDate = new DateTime(2019, 10, 7);
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate);
            var versionning = new Versionning(releaseDate, firstYearWeek);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(4, result.Quarter);
        }

        [Theory]
        [InlineData(2018, 1, 1)]
        [InlineData(2019, 1, 7)]
        [InlineData(2020, 1, 6)]
        [InlineData(2021, 1, 4)]
        public void GivenFirstWeekOfYearShouldReturnZero(int year, int month, int day)
        {
            // Arrange
            var releaseDate = new DateTime(year, month, day);
            var currentCulture = new CultureInfo("fr-FR");
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate.Year, currentCulture);
            var versionning = new Versionning(releaseDate, firstYearWeek, currentCulture);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(0, result.Week);
        }

        [Theory]
        [InlineData(2018, 4, 3)]
        [InlineData(2019, 4, 1)]
        [InlineData(2020, 4, 6)]
        [InlineData(2021, 4, 5)]
        public void GivenFirstWeekOfSecondQuarterShouldReturnZero(int year, int month, int day)
        {
            // Arrange
            var releaseDate = new DateTime(year, month, day);
            var currentCulture = new CultureInfo("fr-FR");
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate.Year, currentCulture);
            var versionning = new Versionning(releaseDate, firstYearWeek, currentCulture);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(0, result.Week);
        }

        [Theory]
        [InlineData(2018, 7, 2)]
        [InlineData(2019, 7, 1)]
        [InlineData(2020, 7, 6)]
        [InlineData(2021, 7, 5)]
        public void GivenFirstWeekOfThirdQuarterShouldReturnZero(int year, int month, int day)
        {
            // Arrange
            var releaseDate = new DateTime(year, month, day);
            var currentCulture = new CultureInfo("fr-FR");
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate.Year, currentCulture);
            var versionning = new Versionning(releaseDate, firstYearWeek, currentCulture);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(0, result.Week);
        }

        [Theory]
        [InlineData(2018, 10, 1)]
        [InlineData(2019, 10, 7)]
        [InlineData(2020, 10, 5)]
        [InlineData(2021, 10, 4)]
        public void GivenFirstWeekOfFourthQuarterShouldReturnZero(int year, int month, int day)
        {
            // Arrange
            var releaseDate = new DateTime(year, month, day);
            var currentCulture = new CultureInfo("fr-FR");
            var firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(releaseDate.Year, currentCulture);
            var versionning = new Versionning(releaseDate, firstYearWeek, currentCulture);

            // Act
            VersionNumber result = versionning.GetVersionNumber();

            // Assert
            Assert.Equal(0, result.Week);
        }
    }
}