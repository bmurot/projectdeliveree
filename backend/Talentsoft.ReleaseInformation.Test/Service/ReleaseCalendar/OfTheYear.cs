﻿namespace Talentsoft.ReleaseInformation.Test.Service.ReleaseCalendar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Service;
    using Xunit;

    public class OfTheYear
    {
        [Fact]
        public void ItExists()
        {
            // Arrange
            var repository = new FakeRepository<ReleaseCalendarInformation>();
            var service = new ReleaseCalendarService(repository);

            // Act & Assert
            var result = service.OfTheYear(2020);
        }

        [Fact]
        public async void GivenYear2020_ShouldReturnDifferentDateOnQualificationDateAndEndDate()
        {
            // Arrange
            var repository = new FakeRepository<ReleaseCalendarInformation>();
            var service = new ReleaseCalendarService(repository);

            // Act
            var result = await service.OfTheYear(2020);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.ReleaseInformation.Count());
            foreach(ReleaseInformation releaseInformation in result.ReleaseInformation)
            {
                Assert.True(releaseInformation.QualificationStartDate == releaseInformation.QualificationEndDate.AddDays(-3));
            }
        }

        [Fact]
        public async void GivenYear2021_ShouldReturnDifferentDateOnQualificationDateAndEndDateUntil14Dot1Dot10AndSameDateForQualificationDateUntilEndOfYear()
        {
            // Arrange
            var repository = new FakeRepository<ReleaseCalendarInformation>();
            var service = new ReleaseCalendarService(repository);

            // Act
            var result = await service.OfTheYear(2021);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.ReleaseInformation.Count());
            ReleaseInformation[] releaseInformationArray = result.ReleaseInformation.ToArray();
            for(var i = 0; i <= 10; i++)
            {
                Assert.True(releaseInformationArray[i].QualificationStartDate == releaseInformationArray[i].QualificationEndDate.AddDays(-3));
            }
        }

        [Fact]
        public async void GivenYear2021_ShouldReturnSameDateOnQualificationDateFrom14Dot1Dot11ToEndOfYear()
        {
            // Arrange
            var repository = new FakeRepository<ReleaseCalendarInformation>();
            var service = new ReleaseCalendarService(repository);

            // Act
            var result = await service.OfTheYear(2021);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.ReleaseInformation.Count());
            ReleaseInformation[] releaseInformationArray = result.ReleaseInformation.ToArray();

            for (var i = 11; i <= 51; i++)
            {
                Assert.True(releaseInformationArray[i].QualificationStartDate == releaseInformationArray[i].QualificationEndDate);
            }
        }

        [Fact]
        public async void GivenYear2022_ShouldReturnSameDateOnQualificationDateAndEndDate()
        {
            // Arrange
            var repository = new FakeRepository<ReleaseCalendarInformation>();
            var service = new ReleaseCalendarService(repository);

            // Act
            var result = await service.OfTheYear(2022);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(52, result.ReleaseInformation.Count());
            foreach (ReleaseInformation releaseInformation in result.ReleaseInformation)
            {
                Assert.True(releaseInformation.QualificationStartDate == releaseInformation.QualificationEndDate);
            }
        }
    }
}