﻿namespace Talentsoft.ReleaseInformation.Test.Service.FirstDayOfWeek
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation;
    using Xunit;

    public class GetFirstWeekOfQuarter
    {
        private readonly CultureInfo _cultureInfo;

        public GetFirstWeekOfQuarter()
        {
            _cultureInfo = new CultureInfo("fr-FR");
        }

        [Fact]
        public void ItExists()
        {
            // Arrange
            DateTime currentDate = DateTime.UtcNow;

            // Act && Assert
            DateTime result = FirstDayOfWeek.GetFirstDayOfFirstWeekOfQuarter(currentDate.Year, currentDate.Month, _cultureInfo);
        }

        [Theory]
        [InlineData("2020-2-12", "2020-1-6")]
        [InlineData("2020-5-13", "2020-4-6")]
        [InlineData("2020-8-20", "2020-7-6")]
        [InlineData("2020-11-16", "2020-10-5")]
        [InlineData("2019-2-13", "2019-1-7")]
        [InlineData("2019-5-17", "2019-4-1")]
        [InlineData("2019-8-21", "2019-7-1")]
        [InlineData("2019-11-18", "2019-10-7")]
        [InlineData("2018-10-28", "2018-10-1")]
        public void GivenADateOfFirstQuarterShouldReturnFirstWeekOfQuarter(DateTime currentDate, DateTime expected)
        {
            // Arrange

            // Act
            DateTime result = FirstDayOfWeek.GetFirstDayOfFirstWeekOfQuarter(currentDate.Year, currentDate.Month, _cultureInfo);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}