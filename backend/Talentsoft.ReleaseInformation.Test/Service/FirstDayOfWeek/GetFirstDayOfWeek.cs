﻿namespace Talentsoft.ReleaseInformation.Test.Service.FirstDayOfWeek
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation;
    using Xunit;

    public class GetFirstDayOfWeek
    {
        [Fact]
        public void ItExists()
        {
            DateTime result = FirstDayOfWeek.GetFirstDayOfWeek(DateTime.UtcNow);
        }

        [Fact]
        public void GivenYear2020ShouldReturnDecemberTheTwelve2019()
        {
            // Act
            DateTime result = FirstDayOfWeek.GetFirstDayOfWeek(new DateTime(2020, 1, 1), new CultureInfo("fr-FR"));

            // Assert
            Assert.Equal(new DateTime(2019, 12, 30), result);
        }
    }
}