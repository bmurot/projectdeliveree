﻿namespace Talentsoft.ReleaseInformation.Test.Service.FirstDayOfWeek
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation;
    using Xunit;

    public class GetFirstDayOfWeekOfYear
    {
        private readonly CultureInfo _cultureInfo;

        public GetFirstDayOfWeekOfYear()
        {
            _cultureInfo = new CultureInfo("fr-FR");
        }

        [Fact]
        public void ItExists()
        {
            DateTime result = FirstDayOfWeek.GetFirstDayOfWeekOfYear(DateTime.UtcNow);
        }

        [Fact]
        public void GivenFirstWeekShouldReturnFirstMondayOfTheYearInACompleteWeekInSameYear()
        {
            // Arrange
            var date = new DateTime(2018, 1, 3);
            var expectedDate = new DateTime(2018, 1, 1);

            // Act
            DateTime result = FirstDayOfWeek.GetFirstDayOfWeekOfYear(date.Year, _cultureInfo);

            // Assert
            Assert.Equal(expectedDate, result);
        }

        [Fact]
        public void GivenADateUnderFirstMondayOfYearShouldReturnFirstMondayOfTheYearInACompleteWeekInSameYear()
        {
            // Arrange
            var date = new DateTime(2020, 1, 3);
            var expectedDate = new DateTime(2020, 1, 6);

            // Act
            DateTime result = FirstDayOfWeek.GetFirstDayOfWeekOfYear(date.Year, _cultureInfo);

            // Assert
            Assert.Equal(expectedDate, result);
        }
    }
}