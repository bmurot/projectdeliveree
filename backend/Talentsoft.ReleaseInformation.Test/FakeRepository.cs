﻿namespace Talentsoft.ReleaseInformation.Test
{
    using MongoDB.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Talentsoft.ReleaseInformation.Repository;

    public class FakeRepository<T> : IRepository<T> where T : Entity
    {
        private readonly List<T> _items = new List<T>();
        private int _id = 0;

        public FakeRepository()
        {
        }

        public string Add(T item)
        {
            item.ID = _id++.ToString();

            _items.Add(item);

            return item.ID;
        }

        public bool Delete(T item)
        {
            throw new NotImplementedException();
        }

        public T Get(string id)
        {
            return _items.FirstOrDefault(i => i.ID == id);
        }

        public IEnumerable<T> GetAll()
        {
            return _items;
        }

        public bool Update(T item)
        {
            throw new NotImplementedException();
        }
    }
}