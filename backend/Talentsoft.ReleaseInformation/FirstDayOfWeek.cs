﻿namespace Talentsoft.ReleaseInformation
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation.Service;

    public static class FirstDayOfWeek
    {
        /// <summary>
        /// Returns the first day of the week that the specified
        /// date is in using the current culture. 
        /// </summary>
        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;

            return GetFirstDayOfWeek(dayInWeek, defaultCultureInfo);
        }

        /// <summary>
        /// Returns the first day of the week that the specified date 
        /// is in. 
        /// </summary>
        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek, CultureInfo cultureInfo)
        {
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        public static DateTime GetFirstDayOfWeekOfYear(DateTime date)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;

            return GetFirstDayOfWeekOfYear(date.Year, defaultCultureInfo);
        }

        public static DateTime GetFirstDayOfWeekOfYear(int year, CultureInfo cultureInfo)
        {
            DateTime date = new DateTime(year, 1, 1);
            DateTime result = GetFirstDayOfWeek(date, cultureInfo);

            if (result.Year < date.Year)
            {
                result = GetFirstDayOfWeek(date.AddDays(7), cultureInfo);
            }

            return result;
        }

        public static DateTime GetFirstDayOfFirstWeekOfQuarter(int year, int month)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;

            return GetFirstDayOfFirstWeekOfQuarter(year, month, defaultCultureInfo);
        }

        public static DateTime GetFirstDayOfFirstWeekOfQuarter(int year, int month, CultureInfo cultureInfo)
        {
            int quarter = QuarterService.ComputeQuarter(month);
            DateTime firstDayOfQuarter = new DateTime(year, (quarter - 1) * 3 + 1, 1);
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;

            while (firstDayOfQuarter.DayOfWeek != firstDay)
            {
                firstDayOfQuarter = firstDayOfQuarter.AddDays(1);
            }

            return firstDayOfQuarter;
        }
    }
}