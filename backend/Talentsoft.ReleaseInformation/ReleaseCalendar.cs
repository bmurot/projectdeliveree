﻿namespace Talentsoft.ReleaseInformation.Service
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Entities;

    public class ReleaseCalendar
    {
        public Task<IEnumerable<ReleaseInformation>> Generate(int year, CultureInfo culture)
        {
            var releaseInformationList = new List<ReleaseInformation>();

            DateTime firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(year, culture);
            var currentReleaseWeek = new DateTime(
                                        firstYearWeek.Year,
                                        firstYearWeek.Month,
                                        firstYearWeek.Day);
            DateTime.SpecifyKind(currentReleaseWeek, DateTimeKind.Local);

            while (currentReleaseWeek.Year <= firstYearWeek.Year)
            {
                var versionning = new Versionning(currentReleaseWeek, firstYearWeek, culture);
                VersionNumber versionNumber = versionning.GetVersionNumber();

                if (!releaseInformationList.Any(r => r.VersionNumber == versionNumber))
                {
                    var releaseInformation = new ReleaseInformation
                    {
                        VersionNumber = versionNumber,
                        ProductionDate = currentReleaseWeek,
                        QualificationStartDate = currentReleaseWeek.AddDays(-7),
                        QualificationEndDate = currentReleaseWeek.AddDays(-4),
                        DateOfQualificationPackageCreation = currentReleaseWeek.AddDays(-10),
                        DevStartDate = currentReleaseWeek.AddDays(-14),
                        DevEndDate = currentReleaseWeek.AddDays(-11)
                    };

                    releaseInformationList.Add(releaseInformation);
                }

                currentReleaseWeek = currentReleaseWeek.AddDays(7);
            }

            return Task.FromResult<IEnumerable<ReleaseInformation>>(releaseInformationList);
        }

        public IEnumerable<ReleaseInformation> GenerateCalendarForOneDayQualProcessFromAStartingDate(DateTime from, CultureInfo culture)
        {
            var releaseInformationList = new List<ReleaseInformation>();

            DateTime firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(from.Year, culture);

            var currentReleaseWeek = FirstDayOfWeek.GetFirstDayOfWeek(from, culture);
            DateTime.SpecifyKind(currentReleaseWeek, DateTimeKind.Local);

            while (currentReleaseWeek.Year <= firstYearWeek.Year)
            {
                var versionning = new Versionning(currentReleaseWeek, firstYearWeek, culture);
                VersionNumber versionNumber = versionning.GetVersionNumber();

                if (!releaseInformationList.Any(r => r.VersionNumber == versionNumber))
                {
                    var releaseInformation = new ReleaseInformation
                    {
                        VersionNumber = versionNumber,
                        ProductionDate = currentReleaseWeek,
                        QualificationStartDate = currentReleaseWeek.AddDays(-3),
                        QualificationEndDate = currentReleaseWeek.AddDays(-3),
                        DateOfQualificationPackageCreation = currentReleaseWeek.AddDays(-3),
                        DevStartDate = currentReleaseWeek.AddDays(-10),
                        DevEndDate = currentReleaseWeek.AddDays(-4)
                    };

                    releaseInformationList.Add(releaseInformation);
                }

                currentReleaseWeek = currentReleaseWeek.AddDays(7);
            }

            return releaseInformationList;
        }

        public IEnumerable<ReleaseInformation> GenerateForFirstPartOfYearToAVersion(VersionNumber toVersionNumber, CultureInfo culture)
        {
            var releaseInformationList = new List<ReleaseInformation>();
            

            DateTime firstYearWeek = FirstDayOfWeek.GetFirstDayOfWeekOfYear(Versionning.GetYearByVersionNumberYear(toVersionNumber.Year), culture);
            var currentReleaseWeek = new DateTime(
                                        firstYearWeek.Year,
                                        firstYearWeek.Month,
                                        firstYearWeek.Day);
            DateTime.SpecifyKind(currentReleaseWeek, DateTimeKind.Local);
            var currentVersionNumber = new VersionNumber
            {
                Year = -1,
                Quarter = -1,
                Week = -1
            };

            do
            {
                var versionning = new Versionning(currentReleaseWeek, firstYearWeek, culture);
                currentVersionNumber = versionning.GetVersionNumber();

                if (!releaseInformationList.Any(r => r.VersionNumber == currentVersionNumber))
                {

                    var releaseInformation = new ReleaseInformation
                    {
                        VersionNumber = currentVersionNumber,
                        ProductionDate = currentReleaseWeek,
                        QualificationStartDate = currentReleaseWeek.AddDays(-7),
                        QualificationEndDate = currentReleaseWeek.AddDays(-4),
                        DateOfQualificationPackageCreation = currentReleaseWeek.AddDays(-10),
                        DevStartDate = currentReleaseWeek.AddDays(-14),
                        DevEndDate = currentReleaseWeek.AddDays(-11)
                    };

                    releaseInformationList.Add(releaseInformation);
                }

                currentReleaseWeek = currentReleaseWeek.AddDays(7);
            }
            while (currentVersionNumber != toVersionNumber);

            return releaseInformationList;
        }
    }
}