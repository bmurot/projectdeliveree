﻿namespace Talentsoft.ReleaseInformation.Cache
{
    using Microsoft.Extensions.Caching.Memory;
    using Talentsoft.ReleaseInformation.Entities;

    public class ReleaseCalendarInformationCache : IReleaseCalendarInformationCache
    {
        private readonly MemoryCache _cache;

        public ReleaseCalendarInformationCache()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public ReleaseCalendarInformation GetForYear(int year)
        {
            _cache.TryGetValue(
                GetCacheKey(year),
                out ReleaseCalendarInformation releaseCalendarInformation);

            return releaseCalendarInformation;
        }

        public void SetForYear(ReleaseCalendarInformation releaseCalendarInformation)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSize(1);
            _cache.Set(GetCacheKey(releaseCalendarInformation.Year),
                releaseCalendarInformation,
                cacheEntryOptions);
        }

        public void Remove(int year)
        {
            _cache.Remove(GetCacheKey(year));
        }

        private string GetCacheKey(int year) => $"ReleaseCalendarInformation-{year}";
    }
}