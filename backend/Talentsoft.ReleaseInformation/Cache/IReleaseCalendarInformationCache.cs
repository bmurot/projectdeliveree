﻿namespace Talentsoft.ReleaseInformation.Cache
{
    using Talentsoft.ReleaseInformation.Entities;

    public interface IReleaseCalendarInformationCache
    {
        ReleaseCalendarInformation GetForYear(int year);

        void SetForYear(ReleaseCalendarInformation releaseCalendarInformation);

        void Remove(int year);
    }
}