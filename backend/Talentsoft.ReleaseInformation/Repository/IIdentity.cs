﻿namespace Talentsoft.ReleaseInformation.Repository
{
    public interface IIdentity
    {
        long ID { get; set; }
    }
}