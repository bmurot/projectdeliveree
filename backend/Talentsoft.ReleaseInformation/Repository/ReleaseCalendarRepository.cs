﻿namespace Talentsoft.ReleaseInformation.Repository
{
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Entities;

    public class ReleaseCalendarRepository : IRepository<ReleaseCalendarInformation>
    {
        private readonly IMongoDatabase _database;

        public ReleaseCalendarRepository(string connectionString, string databaseName)
        {
            var mongoClient = new MongoClient(connectionString);

            _database = mongoClient.GetDatabase(databaseName);
        }

        public string Add(ReleaseCalendarInformation releaseCalendar)
        {
            IMongoCollection<ReleaseCalendarInformation> releaseCalendars = _database.GetCollection<ReleaseCalendarInformation>("ReleaseCalendars");

            releaseCalendars.InsertOne(releaseCalendar);

            return string.Empty;
        }

        public bool Delete(ReleaseCalendarInformation item)
        {
            throw new NotImplementedException();
        }

        public ReleaseCalendarInformation Get(string id)
        {
            IMongoCollection<ReleaseCalendarInformation> releaseCalendars = _database.GetCollection<ReleaseCalendarInformation>("ReleaseCalendars");

            var builder = Builders<ReleaseCalendarInformation>.Filter;
            var filter = builder.Eq("_ id", id);

            return releaseCalendars.Find(filter).FirstOrDefault();
        }

        public IEnumerable<ReleaseCalendarInformation> GetAll()
        {
            IMongoCollection<ReleaseCalendarInformation> releaseCalendars =  _database.GetCollection<ReleaseCalendarInformation>("ReleaseCalendars");

            return releaseCalendars.FindAsync(FilterDefinition<ReleaseCalendarInformation>.Empty).Result.ToList();
        }

        public bool Update(ReleaseCalendarInformation releaseCalendar)
        {
            throw new NotImplementedException();
        }
    }
}