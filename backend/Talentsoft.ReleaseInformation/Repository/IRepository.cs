﻿namespace Talentsoft.ReleaseInformation.Repository
{
    using System.Collections.Generic;

    public interface IRepository<T>
    {
        T Get(string id);

        IEnumerable<T> GetAll();

        string Add(T item);

        bool Update(T item);

        bool Delete(T item);
    }
}