﻿namespace Talentsoft.ReleaseInformation.Service.Interface
{
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Entities;

    public interface IReleaseCalendarService
    {
        bool IsYearAlreadyStored(short year);

        Task<ReleaseCalendarInformation> OfTheYear(short year);

        bool Save(ReleaseCalendarInformation releaseCalendarInformation);

        Task<ReleaseInformation> GetReleaseInformationByVersionNumber(VersionNumber versionNumber);
    }
}