﻿namespace Talentsoft.ReleaseInformation.Service
{
    using System;
    using System.Globalization;
    using Talentsoft.ReleaseInformation.Entities;

    public class Versionning
    {
        private readonly DateTime _releaseDate;
        private readonly CultureInfo _culture = CultureInfo.CurrentCulture;


        public Versionning(DateTime releaseDate, DateTime firstYearWeek)
        {
            _releaseDate = releaseDate;
        }

        public Versionning(DateTime releaseDate, DateTime firstYearWeek, CultureInfo culture) : this(releaseDate, firstYearWeek)
        {
            _culture = culture;
        }

        public VersionNumber GetVersionNumber()
        {
            var versionNumber = new VersionNumber
            {
                Year = ComputeYear(),

                Quarter = ComputeQuarter(),

                Week = ComputeWeek()
            };

            return versionNumber;
        }

        private int ComputeYear()
        {
            int talentsoftYearZero = 2007;

            return _releaseDate.Year - talentsoftYearZero;
        }

        /// <summary>
        /// Compute in which quarter is the release date
        /// </summary>
        /// <returns>Number of quarter</returns>
        private int ComputeQuarter()
        {
            return QuarterService.ComputeQuarter(_releaseDate.Month);
        }

        /// <summary>
        /// Code from https://stackoverflow.com/questions/1497586/how-can-i-calculate-find-the-week-number-of-a-given-date
        /// </summary>
        /// <returns></returns>
        private int ComputeWeek()
        {
            var day = (int)_culture.Calendar.GetDayOfWeek(_releaseDate);

            int weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                        _releaseDate.AddDays(4 - (day == 0 ? 7 : day)),
                        CalendarWeekRule.FirstFourDayWeek,
                        DayOfWeek.Monday);

            DateTime firstQuarterDate = FirstDayOfWeek.GetFirstDayOfFirstWeekOfQuarter(_releaseDate.Year, _releaseDate.Month, _culture);

            // Can not explain why, obtained with TDD
            if (firstQuarterDate.Day >= 5 && DateTime.IsLeapYear(_releaseDate.Year) ||
                firstQuarterDate.Day == 7 && !DateTime.IsLeapYear(_releaseDate.Year))
            {
                weekNumber--;
            }

            // Talentsoft start to zero and not one for week number in versionning
            --weekNumber;

            return weekNumber % 13;
        }

        public static int GetYearByVersionNumberYear(int versionNumberYear)
        {
            return versionNumberYear + 2007;
        }
    }
}