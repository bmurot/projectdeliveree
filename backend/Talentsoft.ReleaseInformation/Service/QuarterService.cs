﻿namespace Talentsoft.ReleaseInformation.Service
{
    public static class QuarterService
    {
        public static int ComputeQuarter(int month)
        {
            return (month - 1) / 3 + 1;
        }
    }
}