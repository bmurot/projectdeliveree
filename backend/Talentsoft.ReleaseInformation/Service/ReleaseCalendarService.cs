﻿namespace Talentsoft.ReleaseInformation.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Talentsoft.ReleaseInformation.Entities;
    using Talentsoft.ReleaseInformation.Repository;
    using Talentsoft.ReleaseInformation.Service.Interface;

    public class ReleaseCalendarService : IReleaseCalendarService
    {
        private readonly IRepository<ReleaseCalendarInformation> _repository;

        public ReleaseCalendarService(
            IRepository<ReleaseCalendarInformation> repository)
        {
            _repository = repository;
        }

        public bool Save(ReleaseCalendarInformation releaseCalendarInformation)
        {
            if (IsYearAlreadyStored(releaseCalendarInformation.Year))
            {
                return false;
            }

            _repository.Add(releaseCalendarInformation);

            return true;
        }

        public bool IsYearAlreadyStored(short year)
        {
            return _repository.GetAll().Any(r => r.Year == year);
        }

        public async Task<ReleaseCalendarInformation> OfTheYear(short year)
        {
            if (!IsYearAlreadyStored(year))
            {
                var culture = new System.Globalization.CultureInfo("fr-FR");
                IEnumerable<ReleaseInformation> releaseInformation = null;

                if (year == 2021)
                {
                    var untilVersion = new VersionNumber
                    {
                        Year = 14,
                        Quarter = 1,
                        Week = 10
                    };
                    IEnumerable<ReleaseInformation> releaseInformationOfBeginningOfTheYear = new ReleaseCalendar().GenerateForFirstPartOfYearToAVersion(untilVersion, culture);

                    IEnumerable<ReleaseInformation> releaseInformationOfTheEndOfTheYear = new ReleaseCalendar().GenerateCalendarForOneDayQualProcessFromAStartingDate(new DateTime(2021, 03, 22), culture);

                    releaseInformation = releaseInformationOfBeginningOfTheYear.Union(releaseInformationOfTheEndOfTheYear);
                }
                else if(year >= 2022) // Over-engineering, but was easy to add. Computed generation could be replaced by getting versions from Jira API
                {
                    releaseInformation = new ReleaseCalendar().GenerateCalendarForOneDayQualProcessFromAStartingDate(FirstDayOfWeek.GetFirstDayOfWeekOfYear(new DateTime(year, 1, 1)), culture);
                }
                else
                {
                    releaseInformation = await new ReleaseCalendar().Generate(year, culture);
                }
                
                var currentReleaseCalendar = new ReleaseCalendarInformation(year, releaseInformation);
                Save(currentReleaseCalendar);
            }

            return _repository.GetAll().SingleOrDefault(r => r.Year == year);
        }

        public Task<ReleaseInformation> GetReleaseInformationByVersionNumber(VersionNumber versionNumber)
        {
            int year = Versionning.GetYearByVersionNumberYear(versionNumber.Year);

            ReleaseCalendarInformation calendarByYear = _repository.GetAll().Where(c => c.Year == year).FirstOrDefault();

            return Task.FromResult(calendarByYear?.ReleaseInformation.Where(r => r.VersionNumber == versionNumber).FirstOrDefault());
        }
    }
}