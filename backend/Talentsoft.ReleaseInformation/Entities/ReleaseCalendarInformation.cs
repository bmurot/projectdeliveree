﻿namespace Talentsoft.ReleaseInformation.Entities
{
    using MongoDB.Entities;
    using System.Collections.Generic;

    public class ReleaseCalendarInformation : Entity
    {
        public ReleaseCalendarInformation(short year, IEnumerable<ReleaseInformation> releaseInformation)
        {
            Year = year;
            ReleaseInformation = releaseInformation;
        }
        public short Year { get; private set; }

        public IEnumerable<ReleaseInformation> ReleaseInformation { get; private set; }
    }
}