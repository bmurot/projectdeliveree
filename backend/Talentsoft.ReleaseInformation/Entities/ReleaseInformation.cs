﻿namespace Talentsoft.ReleaseInformation.Entities
{
    using System;

    public class ReleaseInformation
    {
        public VersionNumber VersionNumber { get; set; }

        public DateTime ProductionDate { get; set; }

        public string Comment { get; set; }

        public DateTime QualificationStartDate { get; set; }

        public DateTime QualificationEndDate { get; set; }

        public DateTime DateOfQualificationPackageCreation { get; set; }

        public DateTime DevStartDate { get; set; }

        public DateTime DevEndDate { get; set; }
    }
}