﻿using System;

namespace Talentsoft.ReleaseInformation.Entities
{
    /// <summary>
    /// Rules of TS versionning => https://talentsoft.atlassian.net/wiki/spaces/AF/pages/20578387/Rules+of+TS+versionning
    /// </summary>

    public class VersionNumber
    {
        public int Year { get; set; }

        public int Quarter { get; set; }

        public int Week { get; set; }

        public int Patch => 0;

        public override string ToString()
        {
            return $"{Year}.{Quarter}.{Week}";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is VersionNumber))
                return false;

            var other = obj as VersionNumber;

            if (Year != other.Year
                || Quarter != other.Quarter
                || Week != other.Week
                || Patch != other.Patch)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Year, Quarter, Week, Patch);
        }

        public static bool operator ==(VersionNumber x, VersionNumber y)
        {
            return x.Equals(y);
        }

        public static bool operator !=(VersionNumber x, VersionNumber y)
        {
            return !(x == y);
        }
    }
}