const rootUrl = process.env.REACT_APP_ENV === 'production'
                        ? 'https://talentsoft-releases-backend-cegid.azurewebsites.net'
                        : process.env.REACT_APP_ENV === 'staging'
                        ? 'https://delivereestaging-backend-cegid.azurewebsites.net'
                        : 'http://localhost:5001';

export function GetInterestingReleaseDates() {
    return fetch(rootUrl +  '/api/ReleaseCalendar/all')
        .then(res => res.json());       
}

export function GetReleaseDatesByYear(year: number) {
    return fetch(rootUrl +  '/api/ReleaseCalendar/'+ year)
    .then(res => res.json()); 
}