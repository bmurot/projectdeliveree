import moment, { Moment } from "moment";

export function GetWeekNumberByDate(date: Moment) {
    let weekNumber: number;

    // La semaine 1 est la semaine du premier jeudi de l'année.
    const firstDayOfYear = moment(`01-01${ date.year() }`, 'MM-DD-YYYY');
    let numberOfDaysbetweenFirstOfJanuaryAndFirstDayOfFirstWeekOfTheYear: number;

    // Calcul de la date du premier lundi de l'année suivant la date reçue.
    if (firstDayOfYear.day() <= 4) {

        // cas de la semaine 1 qui commence fin de l'année précédente ou le 1er janvier - transition de 2019 à 2020 par exemple
        numberOfDaysbetweenFirstOfJanuaryAndFirstDayOfFirstWeekOfTheYear = -firstDayOfYear.day() + 1;  
    }
    else {

        // cas de la semaine 1 qui commence après le 1er janvier - transition de 2020 à 2021 par exemple
        numberOfDaysbetweenFirstOfJanuaryAndFirstDayOfFirstWeekOfTheYear = -firstDayOfYear.day() + 8;
    }

    weekNumber = Math.ceil((date.dayOfYear() - numberOfDaysbetweenFirstOfJanuaryAndFirstDayOfFirstWeekOfTheYear) / 7);
    
    if (weekNumber == 0) weekNumber = 1;

    // Cas de la semaine charnière d'une année à l'autre.
    if (weekNumber == 53) {
        switch (date.year()){
            case 2020:
            case 2026:
            case 2032:
            case 2037:
                break;
            default:
                weekNumber = 1;
        }
    }

    return weekNumber;
}

// tester les fins d'années : 52 ou 53 semaines.
// référence : https://www.calendrier.best/combien-de-semaine-dans-une-annee.html
