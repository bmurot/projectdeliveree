class ReleaseDate {
    releaseDate: string;
    qualificationStartDate: string;
    qualificationEndDate: string;
    devStartDate: string;
    devEndDate: string;
    version: string;
    comment: string;
    dateOfQualificationPackageCreation: string;
    jiraFilterLink: string;

    constructor(releaseDate: string, version: string, comment: string, qualificationStartDate: string, qualificationEndDate: string, devStartDate: string, devEndDate: string, dateOfQualificationPackageCreation: string, jiraFilterLink: string){
        this.releaseDate = releaseDate;
        this.version = version;
        this.comment = comment;
        this.qualificationStartDate = qualificationStartDate;
        this.qualificationEndDate = qualificationEndDate;
        this.devStartDate = devStartDate;
        this.devEndDate = devEndDate;
        this.dateOfQualificationPackageCreation = dateOfQualificationPackageCreation;
        this.jiraFilterLink = jiraFilterLink;
    }

    displayDate(): void {
        console.log("version", this.version);
        console.log("date", this.releaseDate);
    }
}

export default ReleaseDate;