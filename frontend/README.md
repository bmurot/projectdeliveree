# FreeTime TalentSoft Releases

## Installation
Install the packages dependencies
```bash
$ yarn install

# or with npm
$ npm install
```

## Run the project
Launch the server for development 
```bash
$ yarn dev

# or with npm
$ npm run dev
```

## Build the project
Build the project and see the result in `dist/` folder
```bash
$ yarn build

# or with npm
$ npm run build
```


---- 

**Made by Talentsoft**