import * as React from "react";
import * as ReactDOM from "react-dom";
import Home from "./views/Home";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./stores/reducers";
import webConfig from './web.config';

const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <Home />
    </Provider>,
     document.querySelector("#app")
);