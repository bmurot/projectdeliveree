import {combineReducers} from "redux";
import user from "./reducers/user";
import selectedVersion from "./reducers/selectedVersion";
import currentlyIntegratedVersion from "./reducers/currentlyIntegratedVersion";
import allVersions from "./reducers/allVersions";
import allVersionsModeActivated from "./reducers/allVersionsModeActivated";

const rootReducer = combineReducers({
    user,
    selectedVersion,
    currentlyIntegratedVersion,
    allVersions,
    allVersionsModeActivated
});

export default rootReducer;