interface AllVersionsModeActivation {
    type: "SET_ALL_VERSIONS_MODE_ACTIVATED",
    allVersionsModeActivated: boolean
}

export type rootActionsAllVersionsModeActivation = AllVersionsModeActivation;

const allVersionsModeActivated = (state: boolean = false, action: rootActionsAllVersionsModeActivation) => {
    switch (action.type) {
        case "SET_ALL_VERSIONS_MODE_ACTIVATED":
            return {
                // ...state,
                allVersionsModeActivated: action.allVersionsModeActivated,
            };
        default:
            return state;
    }
}

export default allVersionsModeActivated;