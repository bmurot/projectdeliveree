import Moment from "react-moment";
import ReleaseDate from "../../types/ReleaseDate";

// interface AllVersionsState extends Array<ReleaseDate>{}

const allVersionsStateInit: Array<ReleaseDate> =
     null;
    // [
        // {
        //     version: "",
        //     qualificationStartDate: null,
        //     qualificationEndDate: null,
        //     devStartDate: null,
        //     devEndDate: null,
        //     releaseDate: null,
        //     dateOfQualificationPackageCreation: null
        // }
    // ];


interface SetAllVersionsAction {
    type: "SET_VERSIONS_LIST",
    releaseDates: Array<ReleaseDate>
}

export type rootActionsCurrentlyIntegratedVersion = SetAllVersionsAction;

const allVersions = (state: Array<ReleaseDate> = allVersionsStateInit, action: rootActionsCurrentlyIntegratedVersion) => {
    switch (action.type) {
        case "SET_VERSIONS_LIST":
            // action.releaseDates
            return { // faire un map ?
                ...state,
                allVersions: action.releaseDates
            };
        default:
            return state;
    }
}

export default allVersions;