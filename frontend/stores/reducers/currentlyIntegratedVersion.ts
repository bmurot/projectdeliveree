import Moment from "react-moment";
import ReleaseDate from "../../types/ReleaseDate";

export interface VersionState {
    version: string;
    qualificationStartDate: Moment;
    qualificationEndDate: Moment;
    devStartDate: Moment;
    devEndDate: Moment;
    releaseDate: Moment;
    dateOfQualificationPackageCreation: Moment;
    jiraFilterLink: string;
}

const stateInit: VersionState = {
    version: "",
    qualificationStartDate: null,
    qualificationEndDate: null,
    devStartDate: null,
    devEndDate: null,
    releaseDate: null,
    dateOfQualificationPackageCreation: null,
    jiraFilterLink: null
};

interface UserSetCurrentlyIntegratedVersion {
    type: "SET_CURRENTLY_INTEGRATED_VERSION",
    releaseDate: ReleaseDate
}

export type rootActionsCurrentlyIntegratedVersion = UserSetCurrentlyIntegratedVersion;

const currentlyIntegratedVersion = (state: VersionState = stateInit, action: rootActionsCurrentlyIntegratedVersion) => {
    switch (action.type) {
        case "SET_CURRENTLY_INTEGRATED_VERSION":
            return {
                ...state,
                version: action.releaseDate.version,
                qualificationStartDate: action.releaseDate.qualificationStartDate,
                qualificationEndDate: action.releaseDate.qualificationEndDate,
                devStartDate: action.releaseDate.devStartDate,
                devEndDate: action.releaseDate.devEndDate,
                releaseDate: action.releaseDate.releaseDate,
                dateOfQualificationPackageCreation: action.releaseDate.dateOfQualificationPackageCreation,
                jiraFilterLink: action.releaseDate.jiraFilterLink  
            };
        default:
            return state;
    }
}

export default currentlyIntegratedVersion;