export interface UserState {
    name: string;
}

const stateInit: UserState = {
    name: "Toto"
};

interface UserSetName {
    type: "SET_NAME"
}

export type rootActionsUser = UserSetName;

const user = (state: UserState = stateInit, action: rootActionsUser) => {
    switch (action.type) {
        case "SET_NAME":
            return {
                ...state,
                name: "Titi"
            };
        default:
            return state;
    }
}

export default user;