import * as React from 'react';
import { useDispatch} from "react-redux";
import ReleaseDate from '../../types/ReleaseDate';

interface IVersionNumber{
    icon: string;
    title: string;
    description: string;
    alt: string;
    positionNumber: string;
    version: ReleaseDate;
}

export const VersionNumber: React.FC<IVersionNumber> = (props, {icon, title, description, alt, positionNumber, version}) => {

    const classVersionNumber: string[] = ["version-summary"];
    classVersionNumber.push(`version-summary--${props.positionNumber}`);    
    
    const dispatch = useDispatch();
    function onLoad(){
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: props.version});
    }

    return (
        <div className={classVersionNumber.join(" ")} onClick={() => onLoad()}>
            <div className="version-flex">
                <img className="menu-icon" src={props.icon} alt={props.alt}/>
                <h3 className='version-summary-first-line'>{props.title}</h3> 
            </div>
            <div>
                <h3 className='version-summary-second-line'>{props.description}</h3>
            </div>
        </div>
    )
}