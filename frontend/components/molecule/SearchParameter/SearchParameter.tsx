import * as React from 'react'

interface SearchParameterProps {
    ParameterName: string
    PlaceHolder: string
}

export const SearchParameter: React.FC<SearchParameterProps> = ({ParameterName, PlaceHolder }) => {
    return (
        <div className="search-parameter">
            <p className="parameter-name">{ParameterName}</p>
            <p className="place-holder">{PlaceHolder}</p>
        </div>
    )
}