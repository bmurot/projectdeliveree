import * as React from 'react';
import ReleaseDate from '../../../types/ReleaseDate';
import Moment from 'react-moment';
import 'moment-timezone';
import moment = require('moment');
import { useSelector, useDispatch, RootStateOrAny } from "react-redux";
import FileIcon from '../../../assets/images/files.svg';
import { GetWeekNumberByDate } from '../../../services/WeekNumber';

interface IReleaseLine{
    ReleaseDate: ReleaseDate;
}
export const ReleaseLine: React.FC<IReleaseLine> = (props) => {
    const dispatch = useDispatch();

    let momentReleaseDate = moment(props.ReleaseDate.releaseDate);
    let momentQualificationEndDate = moment(props.ReleaseDate.qualificationEndDate);
    let momentIntegrationEndDate = moment(props.ReleaseDate.devEndDate);
    
    let releaseWeekNumber = GetWeekNumberByDate(momentReleaseDate);
    let integrationWeekNumber = GetWeekNumberByDate(momentIntegrationEndDate);
    let qualificationWeekNumber = GetWeekNumberByDate(momentQualificationEndDate);

    function seeVersionDetails(version: ReleaseDate){
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: version});
        let lul = document.getElementById("timeline");
        document.documentElement.scrollTo(0, lul.offsetTop);
        document.body.scrollTo(0, lul.offsetTop);
    }

    return (
        <>
            <tr onClick={() => seeVersionDetails(props.ReleaseDate)}>
                <td className="version-number-td">{props.ReleaseDate.version}</td>
                <td className="integration-period-td">
                    <p className="week-number">Week { integrationWeekNumber }</p>
                    <p className="date"><Moment format="DD/MM/YYYY">{props.ReleaseDate.devStartDate}</Moment> &rarr; <Moment format="DD/MM/YYYY">{momentIntegrationEndDate}</Moment></p>
                </td>

                <td>
                    <p className="week-number">Week { qualificationWeekNumber }</p>
                    {
                        props.ReleaseDate.qualificationStartDate == props.ReleaseDate.qualificationEndDate && 
                        <p className="date"><Moment format="DD/MM/YYYY">{props.ReleaseDate.qualificationStartDate}</Moment></p>
                    }
                    {
                        props.ReleaseDate.qualificationStartDate != props.ReleaseDate.qualificationEndDate && 
                        <p className="date"><Moment format="DD/MM/YYYY">{props.ReleaseDate.qualificationStartDate}</Moment> &rarr; <Moment format="DD/MM/YYYY">{momentQualificationEndDate}</Moment></p>
                    }
                </td>

                <td>
                    <p className="week-number">Week { releaseWeekNumber }</p>
                    <p className="date"><Moment date={momentReleaseDate} format="DD/MM/YYYY"/></p>
                </td>
                <td>
                    {
                        null!= props.ReleaseDate.jiraFilterLink && undefined != props.ReleaseDate.jiraFilterLink &&
                        <a href={props.ReleaseDate.jiraFilterLink} title="Link to Jira Release Note" target="_blank">
                                <button className="releaseline-link-button">
                                    <img className="release-note-file-icon" src={FileIcon} alt="file icon"/>
                                </button>
                        </a>
                    }
                </td>
            </tr>
        </>
        
    )
}