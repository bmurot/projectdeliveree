import * as React from 'react';

interface IAreaTitle {
    firstPart: string;
    secondPart: string;
}

export const AreaTitle: React.FC<IAreaTitle> = (props) => {
    return (
        <h2><strong>{props.firstPart} </strong>{props.secondPart}</h2>
    )
}