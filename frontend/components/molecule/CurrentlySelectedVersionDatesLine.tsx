import * as React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';
import moment = require('moment');
import { RootStateOrAny, useSelector } from 'react-redux';
import ReleaseDate from '../../types/ReleaseDate';
import { GetWeekNumberByDate } from '../../services/WeekNumber';

export const CurrentlySelectedCurrentReleaseLine: React.FC = () => {

    const currentlySelectedVersion: ReleaseDate = useSelector((state: RootStateOrAny) => state.selectedVersion);

    let momentReleaseDate = moment(currentlySelectedVersion.releaseDate);
    let momentQualificationEndDate = moment(currentlySelectedVersion.qualificationEndDate);
    let momentIntegrationEndDate = moment(currentlySelectedVersion.devEndDate);
    
    let releaseWeekNumber = GetWeekNumberByDate(momentReleaseDate);
    let integrationWeekNumber = GetWeekNumberByDate(momentIntegrationEndDate);
    let qualificationWeekNumber = GetWeekNumberByDate(momentQualificationEndDate);

    return (
        <>
            <tr className="dates-alignment">
                <td>
                    <p className="week-number">Week { integrationWeekNumber }</p>
                    <p className="date"><Moment format="DD/MM/YYYY">{currentlySelectedVersion.devStartDate}</Moment> &rarr; <Moment format="DD/MM/YYYY">{momentIntegrationEndDate}</Moment></p>
                </td>

                <td>
                    <p className="week-number">Week { qualificationWeekNumber }</p>
                    {
                        currentlySelectedVersion.qualificationStartDate == currentlySelectedVersion.qualificationEndDate &&
                        <p className="date"><Moment format="DD/MM/YYYY">{currentlySelectedVersion.qualificationStartDate}</Moment></p>
                    }
                    {
                        currentlySelectedVersion.qualificationStartDate != currentlySelectedVersion.qualificationEndDate &&
                        <p className="date"><Moment format="DD/MM/YYYY">{currentlySelectedVersion.qualificationStartDate}</Moment> &rarr; <Moment format="DD/MM/YYYY">{momentQualificationEndDate}</Moment></p>
                    }
                </td>                   

                <td>
                    <p className="week-number">Week { releaseWeekNumber }</p>
                    <p className="date"><Moment date={momentReleaseDate} format="DD/MM/YYYY"/></p>
                </td>
            </tr>
        </>       
    )
}