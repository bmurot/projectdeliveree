import * as React from 'react';

export const MenuFirstLine: React.FC = () => {
    return (
        <div className="menu-first-line">
            <div className = "search-link-square">
                <div className = "glass-zone">
                    <a href="#next-releases">
                        <div className="chevron-down"></div>
                    </a>                       
                </div>
            </div>
            <div className="right-area">
                <h2>Insight on Talentsoft releases</h2>
            </div>
        </div>
    )
}