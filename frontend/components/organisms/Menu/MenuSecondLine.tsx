import * as React from 'react';
import {VersionNumber} from '../../molecule/VersionNumber';
import tag from '../../../assets/images/tag.svg';
import starcalendar from '../../../assets/images/star-calendar.svg';
import { RootStateOrAny, useSelector} from "react-redux";
import ReleaseDate from '../../../types/ReleaseDate';
import moment from 'moment';

export const MenuSecondLine: React.FC = () => {

    const allVersions: Array<ReleaseDate> = useSelector((state: RootStateOrAny) => state.allVersions.allVersions);
    const currentlyIntegratedVersion: ReleaseDate = useSelector((state: RootStateOrAny) => state.currentlyIntegratedVersion);
    const literalCurrentlyIntegratedVersionReleaseDate = moment(currentlyIntegratedVersion.releaseDate).format("ll").toString(); // format LL pour mois écrit en entier
    
    let lastReleasedVersions = allVersions.filter(v => new Date(v.releaseDate) < new Date());
    let lastReleasedVersion = lastReleasedVersions[lastReleasedVersions.length-1];
    let currentVersionLitteral = 'V ' + currentlyIntegratedVersion.version; 
    let previousVersionLitteral = 'V ' + lastReleasedVersion.version;  

    return (
        <div className="menu-second-line">
            <div className="empty-left-space"></div>
            <div className="version-numbers">
                <VersionNumber positionNumber="1" icon={tag} description="In development" title={currentVersionLitteral} version={currentlyIntegratedVersion} alt = "calendar icon"></VersionNumber>
                <VersionNumber positionNumber="2" icon={starcalendar} description="Release date" title={literalCurrentlyIntegratedVersionReleaseDate} version={currentlyIntegratedVersion} alt = "tag icon"></VersionNumber>
                <VersionNumber positionNumber="3" icon={tag} description="Last released" title={previousVersionLitteral} version={lastReleasedVersion} alt = "calendar icon"></VersionNumber>
            </div>
        </div>
    )
}