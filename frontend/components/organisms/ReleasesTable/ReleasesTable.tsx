import * as React from 'react';
import { ReleaseLine } from "../../molecule/ReleaseLine/ReleaseLine";
import ReleaseDate from '../../../types/ReleaseDate';
import { RootStateOrAny, useSelector } from "react-redux";

export const ReleasesTable: React.FC = () => {
    const currentlyIntegratedVersion = useSelector((state: RootStateOrAny) => state.currentlyIntegratedVersion);
    let endingIntDate: string;
    let endingQualDate: string;
    let prodDate: string;
    let startingIntDate: string;
    let startingQualDate: string;

    const allVersions: Array<ReleaseDate> = useSelector((state: RootStateOrAny) => state.allVersions.allVersions);
    let nextReleases = allVersions.filter(v => new Date(v.releaseDate) > new Date());
    let nextRelease = nextReleases[0];

    return (
        <>
        {
            undefined != allVersions && null != allVersions &&
            <table>
                <thead>
                    <tr>
                        <th scope="col">Versions</th>
                        <th scope="col">Integration</th>
                        <th scope="col">Qualification</th>
                        <th scope="col">Production</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        undefined != nextReleases &&
                        <>                       
                            {
                                nextReleases[0] != undefined &&
                                <ReleaseLine ReleaseDate={nextReleases[0]}></ReleaseLine>
                            }
                            {
                                nextReleases[1] != undefined &&
                                <ReleaseLine ReleaseDate={nextReleases[1]}></ReleaseLine>
                            }
                            {
                                nextReleases[2] != undefined &&
                                <ReleaseLine ReleaseDate={nextReleases[2]}></ReleaseLine>
                            }
                            {
                                nextReleases[3] != undefined &&
                                <ReleaseLine ReleaseDate={nextReleases[3]}></ReleaseLine>
                            }
                            {
                                nextReleases[4] != undefined &&
                                <ReleaseLine ReleaseDate={nextReleases[4]}></ReleaseLine>
                            }
                        </>
                    }        
                </tbody>
            </table>       
        }
        </>
    )
}