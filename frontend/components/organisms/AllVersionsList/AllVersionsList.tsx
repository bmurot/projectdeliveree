import * as React from 'react';
import { ReleaseLine } from "../../molecule/ReleaseLine/ReleaseLine";
import ReleaseDate from '../../../types/ReleaseDate';
import { GetReleaseDatesByYear } from '../../../services/DatePickerService';

export const AllVersionsList: React.FC = () => {

    React.useEffect(() => {        
        onDeployYear(new Date().getFullYear());
    }, []);

    let yearsArray: Array<number> = [];
    let yearsToDisplay: Array<number> = [];
    let [loadedYears, setLoadedYears] = React.useState<ReleaseDate[]>();
    
    for (let i = 2020; i <= new Date().getFullYear(); i++){
        yearsArray.push(i);
    }

    function onDeployYear(year: number){
        yearsToDisplay.push(year);
        GetReleaseDatesByYear(year).then(json =>{
            setLoadedYears(json);
        });

        let x = document.getElementsByClassName("year-button");
        for (let i = 0; i < x.length; i++) {
            x[i].classList.remove("year-button-selected");
          }
        document.getElementById(year.toString()).classList.add("year-button-selected");
    }

    const yearButtons = yearsArray.map(year =>
        <button className="year-button" id={year.toString()} key={year} onClick={() => onDeployYear(year)}>{year}</button>
    );

    return (
        <>
        {
            <ul id="year-choice">
                {yearButtons}
            </ul>
        }      
            <table className="all-versions-per-year-table">
                <thead>
                    <tr>
                        <th scope="col">Versions</th>
                        <th scope="col">Integration</th>
                        <th scope="col">Qualification</th>
                        <th scope="col">Production</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {loadedYears != undefined && loadedYears.map(v => {
                        return (<ReleaseLine key={v.version} ReleaseDate={v}></ReleaseLine>);
                    })}
                </tbody>
            </table>

            <div className="page-bottom-buttons">
                <a href="#page-switch-button"><button className="up-button">^</button></a>
            </div>
        </>
    )
}