import * as React from "react";
import ReleaseDate from '../../../types/ReleaseDate';
import ReactGantt from 'gantt-for-react';
import moment = require('moment');
import { CurrentlySelectedCurrentReleaseLine } from "../../molecule/CurrentlySelectedVersionDatesLine";

interface ISelectedVersionCalendar{
    selectedVersion: ReleaseDate;
}

export const SelectedVersionCalendar: React.FC<ISelectedVersionCalendar> = (props) => {
    let releaseDate = moment(props.selectedVersion.releaseDate).format("YYYY-MM-DD").toString();
    let momentQualificationEndDate = moment(props.selectedVersion.qualificationEndDate).format("YYYY-MM-DD").toString();
    let momentIntegrationEndDate = moment(props.selectedVersion.devEndDate).format("YYYY-MM-DD").toString();
    let momentQualificationStartDate = moment(props.selectedVersion.qualificationStartDate).format("YYYY-MM-DD").toString();
    let momentIntegrationStartDate = moment(props.selectedVersion.devStartDate).format("YYYY-MM-DD").toString();

    let today = new Date();
    let integrationProgress: number;    
    let devStartDatePrimitive = (new Date(props.selectedVersion.devStartDate))[Symbol.toPrimitive]('number');
    let devEndDatePrimitive = (new Date(props.selectedVersion.devEndDate))[Symbol.toPrimitive]('number');
    let releaseDatePrimitive = (new Date(props.selectedVersion.releaseDate))[Symbol.toPrimitive]('number');
    let todayPrimitive = (new Date())[Symbol.toPrimitive]('number');

    let qualStartDatePrimitive = (new Date(props.selectedVersion.qualificationStartDate))[Symbol.toPrimitive]('number');
    let qualEndDatePrimitive = (new Date(props.selectedVersion.qualificationEndDate))[Symbol.toPrimitive]('number');

    if (today < new Date(props.selectedVersion.devStartDate)){
        integrationProgress = 0;
    }
    else if (today > new Date(props.selectedVersion.devEndDate)){
        integrationProgress = 100
    }
    else {
        let integrationPercent = (todayPrimitive - devStartDatePrimitive) / (devEndDatePrimitive + 86400000 - devStartDatePrimitive) *100;
        integrationProgress = integrationPercent;
    }

    let qualificationProgress: number;

    if (today < new Date(props.selectedVersion.qualificationStartDate)){
        qualificationProgress = 0;
    }
    else if (today > new Date(props.selectedVersion.qualificationEndDate)){
        qualificationProgress = 100;
    }
    else {
        let qualificationPercent = (todayPrimitive - qualStartDatePrimitive) / (qualEndDatePrimitive + 86400000 - qualStartDatePrimitive) *100;
        qualificationProgress = qualificationPercent;
    }

    let releaseProgress: number;

    if (today < new Date(props.selectedVersion.releaseDate)){
        releaseProgress = 0;
    }
    else if (new Date(props.selectedVersion.releaseDate).getDate() === new Date().getDate()
        && new Date(props.selectedVersion.releaseDate).getMonth() === new Date().getMonth()
        && new Date(props.selectedVersion.releaseDate).getFullYear() === new Date().getFullYear()
    ) {
        releaseProgress = (todayPrimitive - releaseDatePrimitive) / 86400000 *100;
    }
    else {
        releaseProgress = 100;
    }

    let tasks = [
        {
            id: 'Task 1',
            name: 'Integration',
            start: momentIntegrationStartDate,
            end: momentIntegrationEndDate,
            progress: integrationProgress,
            dependencies: ''
        },
        {
            id: 'Task 2',
            name: 'Qualification',
            start: momentQualificationStartDate,
            end: momentQualificationEndDate,
            progress: qualificationProgress,
            dependencies: 'Task 1'
          },
          {
            id: 'Task 3',
            name: 'Release',
            start: releaseDate,
            end: releaseDate,
            progress: releaseProgress,
            dependencies: 'Task 2'
          },
      ];

    return(
        <>
            {
                releaseDate != null && 
                <div>
                        
                    <ReactGantt
                        tasks={tasks}
                        viewMode='Day'
                    />
                    <table className="no-hover">
                        <tbody>
                            <CurrentlySelectedCurrentReleaseLine></CurrentlySelectedCurrentReleaseLine>
                        </tbody>
                    </table>
                </div>
            }
        </>
    );
}