import * as React from "react";
import { useSelector, useDispatch, RootStateOrAny } from "react-redux";
import { ReleasesTable } from "../components/organisms/ReleasesTable/ReleasesTable";
import { AreaTitle } from "../components/molecule/AreaTitle/AreaTitle";
import { MenuSecondLine } from "../components/organisms/Menu/MenuSecondLine";
import { MenuFirstLine } from "../components/organisms/Menu/MenuFirstLine";
import { GetInterestingReleaseDates } from "../services/DatePickerService";
import ReleaseDate from "../types/ReleaseDate";
import { SelectedVersionCalendar } from "../components/organisms/Calendar/SelectedVersionCalendar";
import { AllVersionsList } from "../components/organisms/AllVersionsList/AllVersionsList";
import Loader from 'react-loader-spinner'
import FileIcon from '../assets/images/files.svg'

interface IHomeProps {
    name?: string;
}

const Home: React.FC<IHomeProps> = () => {
    // Declare State
    let [dates, setDates] = React.useState<ReleaseDate[]>();
    let [allVersionsModeActive, setAllVersionsModeActive] = React.useState<boolean>(false);
    const currentlySelectedVersion: ReleaseDate = useSelector((state: RootStateOrAny) => state.selectedVersion);
    const currentlyInDevelopmentVersion: ReleaseDate = useSelector((state: RootStateOrAny) => state.currentlyIntegratedVersion);
    const allVersions: Array<ReleaseDate> = useSelector((state: RootStateOrAny) => state.allVersions);
    const dispatch = useDispatch();
    const [loading, setLoading] = React.useState(true);
    const [error, setError] = React.useState(null);
    const [feedbackVisible, setFeedbackVisible] = React.useState(false);
    const currentDate = new Date();

    React.useEffect(() => { //Fetch data
        GetInterestingReleaseDates()
        .then(json => {
            console.log(json);
            setDates(json);
            setLoading(false);
            dispatch({
                type: "SET_VERSIONS_LIST",
                releaseDates: json
            });
            dispatch({  type: "SET_SELECTED_VERSION",
                releaseDate: json.find(d => isCurrentRelease(d)
            )}); // initialize current version
            dispatch({  type: "SET_CURRENTLY_INTEGRATED_VERSION",
                releaseDate: json.find(d => isCurrentRelease(d)
            )}); // get currently integrated version
        }).catch(err => {        
            console.error(err);
            setError(err);
        });  
    }, []);

    function onNextClick(){
        let currentlySelectedVersionIndex = dates.findIndex(p => p.version == currentlySelectedVersion.version);
        let newDate = dates[currentlySelectedVersionIndex+1];
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: newDate});    
    }

    function onPreviousClick(){
        let currentlySelectedVersionIndex = dates.findIndex(p => p.version == currentlySelectedVersion.version);
        let newDate = dates[currentlySelectedVersionIndex-1];
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: newDate});  
    }

    function onActivateAllVersionsMode(){
        setAllVersionsModeActive(true);
    }
    
    function onDeactivateAllVersionsMode(){
        setAllVersionsModeActive(false);
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: currentlyInDevelopmentVersion});
    }

    function onReload() {
        setAllVersionsModeActive(false);
        dispatch({type: "SET_SELECTED_VERSION", releaseDate: currentlyInDevelopmentVersion});
    }

    function isCurrentRelease(currentRelease: ReleaseDate) {
        return new Date(currentRelease.qualificationStartDate) > currentDate
        && new Date(currentRelease.devStartDate) <= currentDate;
    }

    return (
        <React.Fragment>         
            <div className="grid">
                {feedbackVisible === true &&
                    <div className="page-title--feedback">
                        {
                            feedbackVisible &&
                            <div className="feedback">
                                <div className="feedback-text">
                                    <p>Any improvement ideas for this page ? <a href="mailto:rjaouen@talentsoft.com" title="rjaouen@talentsoft.com"><button>Please send your feedback !</button></a></p>
                                </div>
                                <div className="feedback-close">
                                    <button onClick={() => setFeedbackVisible(false)}>X</button>
                                </div>
                            </div>
                        }
                        {
                            allVersionsModeActive === false &&
                            <div className="page-header--feedback">

                                <div onClick={() => onReload()} className="page-title-text">
                                    <h1 className = 'app-title-h1'>TALENTSOFT</h1>
                                    <h2 className = 'app-title-h2'>Releases</h2>
                                </div>
                                <div>
                                    <button className="page-switch-button" onClick={() => onActivateAllVersionsMode() }>See all versions</button>
                                </div>
                            </div>
                        }
                        {
                            allVersionsModeActive === true &&
                            <div className="page-header--feedback">

                                <div onClick={() => onReload()} className="page-title-text">
                                    <h1 className = 'app-title-h1'>TALENTSOFT</h1>
                                    <h2 className = 'app-title-h2'>Releases</h2>
                                </div>
                                <div>
                                    <button id="page-switch-button" className="page-switch-button" onClick={() => onDeactivateAllVersionsMode() }>Back to main menu</button>
                                </div>
                                
                            </div>
                        }
                    </div>
                }

                {feedbackVisible === false &&
                    <div className="page-title">
                        {
                            feedbackVisible &&
                            <div className="feedback">
                                <div className="feedback-text">
                                    <p>Any improvement ideas for this page ? <a href="mailto:rjaouen@talentsoft.com">Please send your feedback !</a></p>
                                </div>
                                <div className="feedback-close">
                                    <button onClick={() => setFeedbackVisible(false)}>X</button>
                                </div>
                            </div>
                        }
                        {
                            allVersionsModeActive === false &&
                            <div className="page-header">

                                <div onClick={() => onReload()} className="page-title-text">
                                    <h1 className = 'app-title-h1'>TALENTSOFT</h1>
                                    <h2 className = 'app-title-h2'>Releases</h2>
                                </div>
                                <div>
                                    <button className="page-switch-button" onClick={() => onActivateAllVersionsMode() }>See all versions</button>
                                </div>
                            </div>
                        }
                        {
                            allVersionsModeActive === true &&
                            <div className="page-header">

                                <div onClick={() => onReload()} className="page-title-text">
                                    <h1 className = 'app-title-h1'>TALENTSOFT</h1>
                                    <h2 className = 'app-title-h2'>Releases</h2>
                                </div>
                                <div>
                                    <button id="page-switch-button" className="page-switch-button" onClick={() => onDeactivateAllVersionsMode() }>Back to main menu</button>
                                </div>
                                
                            </div>
                        }
                    </div>
                }

                {
                    dates == undefined && 
                    <div className="spinner">
                        <Loader
                            type="TailSpin"
                            color="#00BFFF"
                            height={60}
                            width={60}
                        />
                    </div>
                }        
                {
                    allVersionsModeActive === true && dates != undefined &&
                    <>
                        <div className="all-versions-list">
                        <AreaTitle firstPart="All" secondPart="Releases"></AreaTitle>
                        <AllVersionsList></AllVersionsList>
                        </div>
                    </>
                }

                {
                    allVersionsModeActive === false &&
                    <>
                        <div className="menu">
                        <MenuFirstLine></MenuFirstLine>
                        {
                            undefined != allVersions && null != allVersions &&
                            <MenuSecondLine></MenuSecondLine>
                        }
                        </div>
                        { undefined != currentlySelectedVersion && null != currentlySelectedVersion &&
                        <>
                            <div className='version-dropdown'>
                                {
                                    undefined != dates && 0 != dates.findIndex(p => p.version == currentlySelectedVersion.version) &&
                                    <div className="chevron-left"
                                        onClick={() => onPreviousClick()}>
                                    </div>
                                }
                                {
                                    undefined != dates &&
                                    <div className="version-number-dropdown">
                                        <h3>V {currentlySelectedVersion.version}</h3>
                                    </div>
                                }
                                {
                                    undefined != dates && dates.length - 1 != dates.findIndex(p => p.version == currentlySelectedVersion.version) &&
                                    <div className="chevron-right"
                                        onClick={() => onNextClick()}>
                                    </div>
                                }
                            </div>
                            <div className="selected-version-jira-link">
                            {
                                null != currentlySelectedVersion?.jiraFilterLink &&
                                <a href={currentlySelectedVersion.jiraFilterLink} title="Link to Version Release Note"  target="_blank">
                                    <button>
                                        <div className="button-div">
                                            <img className="release-note-file-icon" src={FileIcon} alt="file icon"/>
                                            <span className="release-note-button-span">Release Note</span>
                                        </div>
                                    </button>
                                </a>
                            }
                            </div>
                            <div className="area-title">
                                <AreaTitle firstPart="Details" secondPart="Timeline"></AreaTitle>
                            </div>
                        </>
                        }
                        <div className="timeline" id="timeline">
                            {
                            undefined != dates &&
                            <SelectedVersionCalendar selectedVersion={currentlySelectedVersion}></SelectedVersionCalendar>
                            }
                        </div>

                        <div className='all-releases-area' id="next-releases">
                            <div className='all-releases-header'>
                                <div className = "all-releases-title">
                                    <AreaTitle firstPart="Next" secondPart="Releases"></AreaTitle>
                                </div>
                            </div>

                            {
                                undefined != allVersions && null != allVersions &&
                                <ReleasesTable></ReleasesTable>
                            }
                        </div>
                    </>
                }                
            </div>
        </React.Fragment>       
    );
};

export default Home;